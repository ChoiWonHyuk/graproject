﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    private AudioSource _audioSources;
    public AudioClip[] audioClip;
    // Start is called before the first frame update
    void Start()
    {
        _audioSources = GetComponent<AudioSource>();
        _audioSources.clip = audioClip[Random.Range(0, audioClip.Length - 1)];
        _audioSources.playOnAwake = false;   
        _audioSources.loop = false;          
        _audioSources.priority = 128;       
        _audioSources.volume = 1;          
        _audioSources.pitch = 1;           
        _audioSources.spatialBlend = 0;     
        _audioSources.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
