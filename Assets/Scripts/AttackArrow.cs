﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackArrow : MonoBehaviour
{
    private AudioSource _audioSources;
    public AudioClip[] audioClip;
    public float CD = 0.1f;
    private float time;
    private HPBar HP;
    // Start is called before the first frame update
    void Start()
    {
        _audioSources = GetComponent<AudioSource>();
        _audioSources.clip = audioClip[Random.Range(0, audioClip.Length - 1)];
        _audioSources.playOnAwake = false;   
        _audioSources.loop = false;          
        _audioSources.priority = 128;       
        _audioSources.volume = 1;          
        _audioSources.pitch = 1;            
        _audioSources.spatialBlend = 0;     
        _audioSources.minDistance = 1;      
        _audioSources.maxDistance = 100;    
        
        _audioSources.rolloffMode = AudioRolloffMode.Linear;
    }


    private float currentSecond = 0;

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if (time < -1.0f) time = -1.0f;
    }
    void OnTriggerEnter(Collider other)
    {
        if ((this.transform.parent.gameObject.tag == "EnermyArrow" &&
            other.transform.root.gameObject.tag == "Player") ||
            (this.transform.parent.gameObject.tag == "PlayerArrow"
            && other.transform.root.gameObject.tag == "Enermy"))
        {
            if (time < 0)
            {
                HP = other.transform.root.gameObject.GetComponentInChildren<HPBar>();
                HP.HealthPoint -= 2;
                GetComponent<AudioSource>().time = 0;
                GetComponent<AudioSource>().Play();
                time = CD;
            }
            
        }
        
    }
}
