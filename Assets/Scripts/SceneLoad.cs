﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
    private AudioSource _audioSources;
    public GameObject stop;
    public GameObject Battle;
    public GameObject Win;
    public GameObject Lose;
    private GameObject[] EnemyArray;
    private GameObject[] PlayerArray;
    private GameObject Camera;
    public string unlock = "level1";
    private float time ,timescale;
    private bool flag, isBattle, once =true;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        PlayerPrefs.SetInt(unlock, 1);
        Camera  = GameObject.Find("CameraControl");
        _audioSources = GetComponent<AudioSource>();
        _audioSources.playOnAwake = false;   
        _audioSources.loop = false;          
        _audioSources.priority = 128;      
        _audioSources.volume = 1;           
        _audioSources.pitch = 1;            
        _audioSources.spatialBlend = 0;    
        _audioSources.minDistance = 1;     
        _audioSources.maxDistance = 100;   
        
        _audioSources.rolloffMode = AudioRolloffMode.Linear;
    }

    // Update is called once per frame
    void Update()
    {
        if (flag == true)
        {
            if (time>0) time -= 1;
            else
            {
                Battle.SetActive(false);
                Time.timeScale = 1;
                flag = false;
                isBattle = true;
            }
        }
        if(isBattle == true)
        {
            EnemyArray = GameObject.FindGameObjectsWithTag("Enermy");
            PlayerArray = GameObject.FindGameObjectsWithTag("Player");
            if (EnemyArray.Length == 0)
            {
                Win.SetActive(true);
                if (once)
                {
                    _audioSources.clip = Resources.Load<AudioClip>("Win");
                    _audioSources.Play();
                    once = false;
                }
                Time.timeScale = 0;
            }
            else if (PlayerArray.Length == 0)
            {
                Lose.SetActive(true);
                if (once)
                {
                    _audioSources.clip = Resources.Load<AudioClip>("Lose");
                    _audioSources.Play();
                    once = false;
                }
                Time.timeScale = 0;
            }
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            if (stop.activeSelf == false) LoadStop();
        }
    }
    public void Click(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void LoadStop()
    {
        timescale = Time.timeScale;
        Time.timeScale = 0;
        stop.SetActive(true);
    }

    public void ReloadStop()
    {
        Time.timeScale = timescale;
        stop.SetActive(false);
    }
    public void Play()
    {
        Destroy(Camera.GetComponent<MousePlace>());
        Destroy(GameObject.Find("Canvas/ScrollRectPanel"));
        Destroy(GameObject.Find("Canvas/Go"));
        Battle.SetActive(true);
        flag = true;
        time = 24.0f;
    }
}
