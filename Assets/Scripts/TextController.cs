using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextController : MonoBehaviour
{
    public Text ChatText; // 실제 채팅이 나오는 텍스트
    public Text CharacterName; // 캐릭터 이름이 나오는 텍스트


    public List<KeyCode> skipButton; // 대화를 빠르게 넘길 수 있는 키

    public string writerText = "";

    bool isButtonClicked = false;
    public int line;
    public GameObject Panel1;
    public GameObject Panel2;


    void Start()
    {
        StartCoroutine(TextPractice());
        Panel2.SetActive(false);
    }

    void Update()
    {
        foreach (var element in skipButton) // 버튼 검사
        {
            if (Input.GetKeyDown(element))
            {
                isButtonClicked = true;
            }
        }
        if(line == 2)
        {
            Panel1.SetActive(false);
            Panel2.SetActive(true);
        }
        if (line == 3)
        {
            Panel1.SetActive(true);
            Panel2.SetActive(false);
        }
        if (line == 4)
        {
            Panel1.SetActive(false);
            Panel2.SetActive(true);
        }
        if (line == 5)
        {
            Panel1.SetActive(true);
            Panel2.SetActive(false);
            if(Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene("Tutorial");
            }
        }
    }


    IEnumerator NormalChat(string narrator, string narration)
    {
        int a = 0;
        CharacterName.text = narrator;
        writerText = "";

        //텍스트 타이핑 효과
        for (a = 0; a < narration.Length; a++)
        {
            writerText += narration[a];
            ChatText.text = writerText;
            yield return new WaitForSeconds(0.05f);
        }

        //키를 다시 누를 떄 까지 무한정 대기
        while (true)
        {
            if (isButtonClicked)
            {
                isButtonClicked = false;
                line += 1;
                break;
            }
            yield return null;
        }
    }

    IEnumerator TextPractice()
    {
        yield return StartCoroutine(NormalChat("아레스", "요즘 인간들은 나약하기만 하고 은혜를 전혀 모르고 악의적인 행동들만 하는군. 더 이상 이런 우매한 인간들에게 지구를 맡길 필요가 없는 것 같다."));
        yield return StartCoroutine(NormalChat("아레스", "인간들을 말살하는게 지구에 도움이 되겠군. 처리하도록 하지"));
        yield return StartCoroutine(NormalChat("인간 대표", "말도 안됩니다 아레스님! 저희에게 가능성을 증명할 마지막 기회를 한번만 주십시오!"));
        yield return StartCoroutine(NormalChat("아레스", "너희 같은 우매한 것들이? 무슨 방법으로 너희의 가능성을 증명하겠다는거지?"));
        yield return StartCoroutine(NormalChat("인간 대표", ". . . 그렇다면 저희의 전력을 보여드린다면 기회를 주시겠습니까?"));
        yield return StartCoroutine(NormalChat("아레스", "하하하, 재밌구만. 그래, 한번 내 병사들을 이겨보도록 해봐라"));
    }
}