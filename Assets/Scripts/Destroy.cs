﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    private AudioSource _audioSources;
    public AudioClip[] audioClip;
    private float time=20.0f;
    public bool flag = false, once = true;
    // Start is called before the first frame update
    void Start()
    {
        _audioSources = GetComponent<AudioSource>();
        _audioSources.clip = audioClip[Random.Range(0, audioClip.Length - 1)];
        _audioSources.playOnAwake = false;   
        _audioSources.loop = false;         
        _audioSources.priority = 128;       
        _audioSources.volume = 1;          
        _audioSources.pitch = 1;            
        _audioSources.spatialBlend = 0;     
        _audioSources.minDistance = 1;      
        _audioSources.maxDistance = 100;    
        
        _audioSources.rolloffMode = AudioRolloffMode.Linear;
    }

    // Update is called once per frame
    void Update()
    {
        if (flag&&Time.timeScale>0)
        {
            if (once)
            {
                _audioSources.Play();
                once = false;
            }
            if (time > 0) time -= Time.timeScale;
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
