﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public GameObject Player; 
    private Vector3 offset;   
    private Transform playerTransform;  
    private Transform cameraTransform;  
    public float distance = 0;
    public float scrollSpeed = 10;


    // Use this for initialization
    void Start()
    {
        playerTransform = Player.GetComponent<Transform>(); 
        cameraTransform = this.GetComponent<Transform>();   
        offset = cameraTransform.position - playerTransform.position;  
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = playerTransform.position + offset;  
        ScrollView();
    }
    void ScrollView()
    {
        //print (Input.GetAxis ("Mouse ScrollWheel"));
        distance = offset.magnitude;
        distance -= Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;  
        if (distance > 26)
        {   
            distance = 26;
        }
        if (distance < 5)
        {    
            distance = 5;
        }
        offset = offset.normalized * distance;


    }
}
