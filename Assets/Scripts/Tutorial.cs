using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    public GameObject tutorial1;
    public GameObject tutorial2;
    public GameObject tutorial3;
    public GameObject tutorial4;
    public GameObject tutorial5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void change1()
    {
        tutorial1.SetActive(false);
        tutorial2.SetActive(true);
    }

    public void change2()
    {
        tutorial2.SetActive(false);
        tutorial3.SetActive(true);
    }

    public void change3()
    {
        tutorial3.SetActive(false);
        tutorial4.SetActive(true);
    }

    public void change4()
    {
        tutorial4.SetActive(false);
        tutorial5.SetActive(true);
    }

    public void change5()
    {
        SceneManager.LoadScene("RealTutorial");
    }
}
