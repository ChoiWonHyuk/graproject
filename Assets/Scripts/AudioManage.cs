﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManage : MonoBehaviour
{
    public AudioMixer audioMixer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetMasterVolume(float volume)   
    {
        audioMixer.SetFloat("Master", volume);
        
    }
    public void SetMusicVolume(float volume)   
    {
        audioMixer.SetFloat("Music", volume);
        
    }

    public void SetSoundEffectVolume(float volume)   
    {
        audioMixer.SetFloat("Effect", volume);
       
    }
}
